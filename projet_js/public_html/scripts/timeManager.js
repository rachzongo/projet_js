var compteur;
var duree;
let s, m;
var on;

function initTimer(delai) {
    compteur = document.getElementById('timer');
    on = true;
    duree = delai;
    timer();
    //requestAnimationFrame(timer);
}

/*
 * Calcul et affichage du temps restant
 */
function timer() {
    s = duree;
    m = 0;
    if (s <= 0){
        compteur.innerHTML = "Termin&eacute;";
        stop(0);
        return;
    } else{
        if (s > 59){
            m = Math.floor(s / 60);
            s = s - m * 60;
        }
        if (s < 10){
            s = "0" + s;
        }
        if (m < 10){
            m = "0" + m;
        }
        compteur.innerHTML = m + ":" + s;
    }
    if(on === true)
        duree = duree - 1;

    window.setTimeout("timer();", 1000);
}
/*
 * Arret du temps lorsque la partie est gagnée
 */
function timePause(){
    on = false;
}
