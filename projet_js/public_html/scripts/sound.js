var file;


function playSound(etat){
    switch(etat) { 
        case 0 : //l partie finie et il a perdu
            file = "./rsc/sons/perdu.wav";
            break;
        case 1 : //si La partie est fini et il a gagné
            file = "./rsc/sons/gagne.wav";
            break;
        case 2 : //si Dans le Bon endroit
            file = "./rsc/sons/bonEndroit.wav";
            break;
        default :
            break;
    }
    var audio = new Audio(file);
    audio.play();
}