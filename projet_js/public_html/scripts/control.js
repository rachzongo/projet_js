var data;
var res;

function initControl(sol) {
    res = sol;
    check();
}
/*
 * Verifie si des fragments sont bien placés et les marque
 * 
 */
function check() {
    data = document.querySelectorAll('.draggable');
    datar = document.querySelectorAll('.solved');
    if (datar.length === data.length * 2) {
        stop(1);
        return;
    } else {
        for (i = 0; i < data.length; i++) {
            if (data[i].style.backgroundImage == "url(\"" + res[i] + "\")") {
                //playSound(2);
                var el = data[i];
                var par = el.parentNode;
                par.className = "dropper solved";
                el.className = "draggable solved";
            }
        }
        removeListeners();
        window.setTimeout("check();", 1000);
    }
}

/*
 * Suppresions de listeners sur les elements resolus
 * src : https://stackoverflow.com/questions/19469881/remove-all-event-listeners-of-specific-type 
 * 
 */
function removeListeners() {
    var resolu = document.querySelectorAll('.solved');
    for (i = 0; i < resolu.length; i++) {
        var el = resolu[i];
        var elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);
    }
}
/*
 * Suppresions de tous listeners, meme ceux des elements non resolus 
 * 
 */
function removeAllListeners() {
    var resolu1 = document.querySelectorAll('.draggable');
    for (i = 0; i < resolu1.length; i++) {
        var el = resolu1[i];
        var elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);
    }
    var resolu2 = document.querySelectorAll('.dropper');
    for (i = 0; i < resolu2.length; i++) {
        var el = resolu2[i];
        var elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);
    }
}

