window.onload = load;

var questions;
var index;

/*
 * Lancement du jeu
 */
function load() {
    var request = new XMLHttpRequest();
    request.open("GET", "./rsc/images/bd.json", false);
    request.send();
    var json = JSON.parse(request.responseText);
    questions = json.questions;
    index = 0;
    loadGame();
}

/*
 * Lancement de la partie suivante
 */
function getNextQuestion(){
    if(index >= questions.length - 1)
        index = 0;
    else
        index ++;
    loadGame();
}
/*
 * Chargement des elements et initialisation de la partie
 */
function loadGame(){
    let question = questions[index];
    var solution = question.fragments;
    
    var div = document.getElementById('fragments');
    //On vide le conteneur de fragments :
    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }
    document.getElementById('solution').setAttribute("src", question.img);
    document.getElementById('titre').innerHTML = question.titre;
    
    var width = question.taille[0] * 100 + "px";
    var height = question.taille[1] * 100 + "px";
    div.setAttribute("style", "width: " + width +"; height : " + height);
    
    //console.log("sol bef: "+ solution);
    var MixedImg = swap(solution);
    //console.log("sol aft: "+ solution);
    
    //console.log("mel :" +MixedImg);
    for( i = 0; i < MixedImg.length; i++){
        var dropper = document.createElement('div');
        dropper.className = "dropper";
        var el = document.createElement('div');
        el.className = "draggable";
        el.setAttribute("style", "background-image: url('"+ MixedImg[i] +"')");
        dropper.appendChild(el);
        div.appendChild(dropper);
    }
    initTimer(question.duree);
    initControl(solution);
    initEvents();
}

/*
 * Melange des fragments de l'image
 * @param {type} list
 * @returns {swap.tmp}
 */
function swap(list){  
    var tmp = list.slice();
    for(i = 0; i < tmp.length; i++){
        var a = Math.round((tmp.length - 1) * Math.random());
        var b = Math.round((tmp.length - 1) * Math.random());
        var c = tmp[a];
        tmp[a] = tmp[b];
        tmp[b] = c;
    }
    return tmp;
}

function reload(){
    loadGame();
}

/* 
 * Arret de la partie en cours
 */
function stop(etat){
    if(etat == 0){
        playSound(0);
        alert("Le temps s'est ecoule. Vous avez perdu");
        removeAllListeners();
    }
    else if(etat == 1){
        playSound(1);
        timePause();
        alert("Bravo, vous avez resolu le puzzle");
        removeAllListeners();
    }
}