# Projet Noël - Technos Web - L3 Miage #

### Speed Puzzle ###
Notre projet porte sur un puzzle.


### Comment ça marche??? ###
Le principe du puzzle est de reconstituer une image donnée avec des fragments disposés aléatoirement.
Le jeu consiste à faire glisser un fragment vers un autre pour les faire permuter.
Un fragment bien placé n'est plus déplaçable.
Ainsi en faisant permuter les fragments deux à deux, on arrive à reconstruire l'image.

Le joueur dispose d'un délai pour résoudre un puzzle.
Une fois ce délai passé l'utilisateur la partie est suspendue.

### Auteurs ###
* BOUKADIDA Imen
* ZONGO Rachida
